#!/bin/bash

yum update -y
yum install epel-release -y
yum groupinstall "Server with GUI" -y
yum groupinstall "MATE Desktop" -y
systemctl set-default graphical.target
systemctl isolate graphical.target

yum install tigervnc-server -y

useradd vncuser

prog=/usr/bin/vncpasswd
pass="Ghbpvf107"

/usr/bin/expect <<EOF
spawn "$prog"
expect "Password:"
send "$pass\r"
expect "Verify:"
send "$pass\r"
expect eof
exit
EOF

cp vncserver@:1.service /etc/systemd/system/vncserver@:1.service
systemctl enable vncserver@:1.service
systemctl start vncserver@:1.service

firewall-cmd --add-port=5901/tcp --permanent
firewall-cmd --reload